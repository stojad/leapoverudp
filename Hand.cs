using UnityEngine;
using System.Collections;

namespace UDPLeap {
	public class Hand {
		/* PUBLIC PROPERTIES */
		
		public Arm Arm;

		// Not sure if this is how the hand basis is calculated. Will need to check. It annoyingly isn't given by the data.
		public Matrix Basis
		{
			get
			{
				if(IsRight)
					return new Matrix(PalmNormal.Cross(Direction), -PalmNormal, -Direction);
				else
					return new Matrix(-PalmNormal.Cross(Direction), -PalmNormal, -Direction);
			}
		}

		public float Confidence;
		public Vector Direction;
		public FingerList Fingers = new FingerList();
		public Frame Frame;
		public float GrabStrength;
		public int Id;
		
		public Hand Invalid
		{
			get
			{
				return new Hand();
			}
		}

		public bool IsLeft = false;
		public bool IsRight = false;
		public bool IsValid = false;
		public Vector PalmNormal;
		public Vector PalmPosition;
		public Vector PalmVelocity;
		public float PalmWidth = 80;
		public float PinchStrength;
		public PointableList Pointables = new PointableList();
		public Vector SphereCenter;
		public float SphereRadius;
		public Vector StabilizedPalmPosition;
		public float TimeVisible;
		public ToolList Tools = new ToolList();
		public Vector WristPosition;
		
		/* IMPLEMENTATION-SPECIFIC PROPERTIES */
		
		public Matrix r;
		public float s;
		public Vector t;
	    
	    /* PUBLIC METHODS */
	    
	    // Creates an invalid hand.
	    public Hand() {
	    	
	    }
	    
	    // Returns true if this hand has equal ID and frame ID, and both hands are valid.
	    public bool Equals(Hand arg0) {
	    	return (IsValid && arg0.IsValid & Id == arg0.Id && Frame.Id == arg0.Frame.Id);
	    }
	    
	    // Returns a finger with a specified ID if it exists, otherwise returning an invalid finger.
	    public Finger Finger(int id) {   	
	    	foreach (Finger finger in Fingers) {
	    		if(finger.Id == id) {
	    			return finger;
	    		}
	    	}
	    	
			// Return an invalid finger if it's not in the list.
	    	return new Finger();
	    }
	    
	    // Returns a pointable with a specified ID if it exists, otherwise returning an invalid finger.
	    public Pointable Pointable(int id) {   	
	    	foreach (Pointable pointable in Pointables) {
	    		if(pointable.Id == id) {
	    			return pointable;
	    		}
	    	}
	    	
			// Return an invalid Pointable if it's not found in the list.
	    	return new Pointable();
	    }
	    
	 	// Finds the rotation angle of this hand between two frames. HIGHLY EXPERIMENTAL.
	    public float RotationAngle(Frame sinceFrame) {
			// Theory is from https://developer.leapmotion.com/documentation/javascript/supplements/Leap_JSON.html
			float angle;
			Vector3 axis;
			
			Hand sinceHand = new Hand();
			
			// Get hand from previous sinceFrame. 
			// TODO: Rewwrite this to use the Frame.Hand(int index) method
			
			foreach (Hand hand in sinceFrame.Hands) {
				if(hand.Id == Id) {
					sinceHand = hand;
				}
			}
			
			if(!sinceHand.IsValid) {
				return 0.0f;
			}
			
			Matrix rotationFactor = r * sinceHand.r.RigidInverse();
			Quaternion q = rotationFactor.QuaternionFromMatrix();
			q.ToAngleAxis(out angle, out axis);	
			return angle;
		}
		
		// Not implemented untl I figure out how to do it.
		 public float RotationAngle(Frame sinceFrame, Vector axis) {
			Debug.LogError ("Hand.RotationAngle(Frame sinceFrame, Vector axis) has not yet been implemented in UDPLeap.");
			return 0.0f;
		 }
		 
		 // Determine the angle around which the hand was rotated to get to the current position from the position in sinceFrame.
		 public Vector RotationAxis(Frame sinceFrame) {
			// Theory is from https://developer.leapmotion.com/documentation/javascript/supplements/Leap_JSON.html
			float angle;
			Vector3 axis;
			
			Hand sinceHand = new Hand();
			
			// Get hand from previous sinceFrame. 
			// TODO: Rewwrite this to use the Frame.Hand(int index) method
			
			foreach (Hand hand in sinceFrame.Hands) {
				if(hand.Id == Id) {
					sinceHand = hand;
				}
			}
			
			if(!sinceHand.IsValid) {
				return new Vector(0, 0, 0);
			}
			
			Matrix rotationFactor = r * sinceHand.r.RigidInverse();
			Quaternion q = rotationFactor.QuaternionFromMatrix();
			q.ToAngleAxis(out angle, out axis);	
			return new Vector(axis);
		}
		
		// Return the rotation matrix between two hands.
		public Matrix RotationMatrix(Frame sinceFrame) {
			Hand sinceHand = new Hand();
			
			// Get hand from previous sinceFrame. 
			// TODO: Rewwrite this to use the Frame.Hand(int index) method
			
			foreach (Hand hand in sinceFrame.Hands) {
				if(hand.Id == Id) {
					sinceHand = hand;
				}
			}
			
			if(!sinceHand.IsValid) {
				return new Matrix();
			}
			
			return  r * sinceHand.r.RigidInverse();
		}
	}
}
