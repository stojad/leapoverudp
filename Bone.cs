﻿using UnityEngine;
using System.Collections;

namespace UDPLeap {
	public class Bone {
		/* PUBLIC PROPERTIES */

		public Matrix Basis;
		public Vector Center;
		public Vector Direction;
		public bool IsValid = false;
		public float Length;
		public Vector NextJoint;
		public Vector PrevJoint;
		public BoneType Type;
		public float Width;
		public enum BoneType {TYPE_METACARPAL, TYPE_PROXIMAL, TYPE_INTERMEDIATE, TYPE_DISTAL};

		/* PUBLIC METHODS */

		// Constructs an invalid bone.
		public Bone() {


		}

		// Checks that two bones are equal. They should technically have an Id/Frame but this type doesn't store either.
		public bool Equal(Bone arg0) {
			return (IsValid && arg0.IsValid && Type == arg0.Type);
		}

		// Provides a string representation of the bone.
		public override string ToString() {
			return "[Bone] Type: " + Type + ", width: " + Width;
		}
	}
}
