using UnityEngine;
using System.Collections;

namespace UDPLeap {
	public class Finger : Pointable {
		/* PUBLIC PROPERTIES */

		new public Finger Invalid 
		{
			get
			{
				return new Finger();
			}
		}

		public FingerType Type;

		/* IMPLEMENTATION-SPECIFIC PROPERTIES */

		// Contains an ordered list of the bones for this finger.
		public ArrayList bones = new ArrayList();

		/* PUBLIC METHODS */

		// Construct an invalid Finger.
		public Finger() {


		}

		// Construct a Finger from a Pointable object.
		public Finger(Pointable arg0) {
			// Create an invalid Finger object if the Pointable arg0 is not a Finger.
			if (!arg0.IsValid || !arg0.IsFinger) {
				Debug.LogError ("Attempted to create finger from non-finger pointable. ID: " + arg0.Id);
				IsValid = true;
				return;
			}

			// Copy all fields.
			Direction = arg0.Direction;
			Frame = arg0.Frame;
			Hand = arg0.Hand;
			Id = arg0.Id;
			IsExtended = arg0.IsExtended;
			IsFinger = arg0.IsFinger;
			IsTool = arg0.IsTool;
			IsValid = arg0.IsValid;
			Length = arg0.Length;
			StabilizedTipPosition = arg0.StabilizedTipPosition;
			TimeVisible = arg0.TimeVisible;
			TipPosition = arg0.TipPosition;
			TipVelocity = arg0.TipVelocity;
			TouchDistance = arg0.TouchDistance;
			TouchZone = arg0.TouchZone;
			Width = arg0.Width;
		}

		// Return a specified bone of this finger of type BoneIx
		public Bone Bone(Bone.BoneType boneIx) {
			// This requires that bones are stored in the  list with order METACARPAL, PROXIMAL, INTERMEDIATE, DISTAL.
			if(bones.Count < 4) {
				Debug.LogError("Pointable " + Id + " has " + bones.Count + " bones, but should have 4. (Is finger: " + IsFinger + ")");
			}
			return (Bone) bones[(int) boneIx];
		}

		// An enumeration of the finger types.
		public enum FingerType {TYPE_THUMB, TYPE_INDEX, TYPE_MIDDLE, TYPE_RING, TYPE_PINKY};
	}
}
