﻿using UnityEngine;
using System.Collections;
using System;

namespace UDPLeap {
	public class ToolList {
		/* PUBLIC PROPERTIES */
		
		public int Count
		{
			get
			{
				return toolList.Count;
			}
		}
		
		public bool IsEmpty
		{
			get
			{
				return (toolList.Count == 0);
			}
		}
		
		// Tool with smallest z co-ordinate
		public Tool Frontmost
		{
			get
			{
				Tool frontmost = new Tool();
				float minZ = float.MaxValue;
				float currentZ;
				
				foreach (Tool tool in toolList) {
					currentZ = tool.TipPosition.z;
					if (currentZ < minZ) {
						minZ = currentZ;
						frontmost = tool;
					}
				}
				
				return frontmost;
			}
		}
		
		// Tool with smallest x co-ordinate
		public Tool Leftmost
		{
			get
			{
				Tool leftmost = new Tool();
				float minX = float.MaxValue;
				float currentX;
				
				foreach (Tool tool in toolList) {
					currentX = tool.TipPosition.x;
					if (currentX < minX) {
						minX = currentX;
						leftmost = tool;
					}
				}
				
				return leftmost;
			}
		}
		
		// Hand with largest x co-ordinate
		public Tool Rightmost
		{
			get
			{
				Tool rightmost = new Tool();
				float maxX = float.MinValue;
				float currentX;
				
				foreach(Tool tool in toolList) {
					currentX = tool.TipPosition.x;
					if (currentX > maxX) {
						maxX = currentX;
						rightmost = tool;
					}
				}
				
				return rightmost;
			}
		}
		
		/* IMPLEMENTATION-SPECIFIC PROPERTIES */
		
		public ArrayList toolList;
		
		/* PUBLIC METHODS */
		
		// Constructor creates a new empty list of Tools.
		public ToolList() {
			toolList = new ArrayList ();
		}
		
		// Makes an array accessor.
		public Tool this[int index] {
			get {
				try {
					return (Tool) toolList[index];
				} catch (ArgumentOutOfRangeException) {
					// Return an invalid object if we're out of range.
					return new Tool();
				}
			}
		}

		// Return an enumerator on this list.
		public IEnumerator GetEnumerator()
		{
			return toolList.GetEnumerator();
		}

		// Add an item to the Tool list.
		public ToolList Append(Tool tool) {
			toolList.Add (tool);
			
			return this;
		}
		
		// Appends the items of otherList to the end of this list.
		public ToolList Append(ToolList otherList) {
			toolList.AddRange (otherList.toolList);
			
			return this;
		}
	}
}
