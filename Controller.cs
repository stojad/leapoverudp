using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.IO;
using System.Threading;


namespace UDPLeap {
	public class Controller {
		/* PUBLIC PROPERTIES */

		public bool IsConnected = true;
		public bool HasFocus = true;
		public enum PolicyFlag
		{
			POLICY_DEFAULT = 0,
			POLICY_BACKGROUND_FRAMES = (1 << 0),
			POLICY_IMAGES = (1 << 1),
			POLICY_OPTIMIZE_HMD = (1 << 2)
		};
		// public DecviceList Devices;
		// public TrackedQuad TrackedQuad;
		// public ImageList Images;
		// public Config Config;

		/* IMPLEMENTATION-SPECFIIC PROPERTIES */

		public const int listenPort = 4000;
		public const int sendPort = 4001;
		private UdpClient udpServer, udpClient;
		private IPEndPoint remoteEP, EP;
		private const int MAX_HISTORY_SIZE = 60; 
		private Frame[] frameHistory = new Frame[MAX_HISTORY_SIZE];
		private int currentFront = 0;
		private Dictionary<Gesture.GestureType, bool> enabledGestures = new Dictionary<Gesture.GestureType, bool>();
		private Dictionary<Controller.PolicyFlag, bool> setPolicies = new Dictionary<Controller.PolicyFlag, bool>();
		private static bool dataReady;

		/* PUBLIC METHODS */

		// Constructor creates a UDP client that listens for JSON tracking data over UDP
		public Controller() {
			// Set up the conneciton server from which we receive JSON messages.
			udpServer = new UdpClient();
			udpServer.Client.SetSocketOption(
				SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
			remoteEP = new IPEndPoint(IPAddress.Any, listenPort);
			udpServer.Client.Bind (remoteEP);

			// Set up the client connection so that we can send JSON messages back to the server.
			udpClient = new UdpClient();
			udpClient.EnableBroadcast = true;
			EP = new IPEndPoint(IPAddress.Broadcast, sendPort);
			udpClient.Connect(EP);

			Frame();

			// We want to enable background frames by default.
			SetPolicy (Controller.PolicyFlag.POLICY_BACKGROUND_FRAMES);
			Debug.Log ("Background frames set: " + IsPolicySet(Controller.PolicyFlag.POLICY_BACKGROUND_FRAMES));

			// Test out enabling the circle gesture.
			EnableGesture (Gesture.GestureType.TYPE_CIRCLE);
			Debug.Log ("Circle gestures enabled: " + IsGestureEnabled (Gesture.GestureType.TYPE_CIRCLE));
		}

		// Controllers with listeners are not implemented.
		public Controller(Listener listener) {
			Debug.LogError ("Controller constructors with attached listeners are not implemented by UDPLeap. Listeners are not supported; use polling to get frame and event data.");
		}

		// As listeners are not implemented, you cannot attach a listener to a controller in UDPLeap.
		public bool AddListener(Listener listener) {
			Debug.LogError ("Controller.addListener(Listener listener) is not implemented by UDPLeap. Listeners are not supported; use polling to get frame and event data.");

			return false;
		}

		// Not implemented as listeners are not implemented.
		public bool RemoveListener(Listener listener) {
			Debug.LogError ("Controller.RemoveListener(Listener listener) is not implemented as listeners are not implemented in UDPLeap.");
			
			return false;
		}

		// Set a policy flag. Sends a JSON message to the server to indicate that the flag has been set.
		public void SetPolicy(Controller.PolicyFlag policy) {
			String message;

			switch (policy) {
			case Controller.PolicyFlag.POLICY_BACKGROUND_FRAMES:
				message = "{background: true}";
				break;
			case Controller.PolicyFlag.POLICY_OPTIMIZE_HMD:
				message = "{optimizeHMD: true}";
				break;
			default:
				Debug.LogError("Unsupported policy flag " + policy + " set. Ignoring.");
				return;
			}

			// Send data to server
			Byte[] sendBytes = Encoding.UTF8.GetBytes(message);
			udpClient.Send (sendBytes, sendBytes.Length);

			// Set internal state
			if (!setPolicies.ContainsKey (policy)) {
				setPolicies.Add (policy, true);
			} else {
				setPolicies[policy] = true;
			}
		}

		// Clears a policy, sending a message to the server indicating that the policy should be clared.
		public void ClearPolicy(Controller.PolicyFlag policy) {
			String message;

			switch (policy) {
			case Controller.PolicyFlag.POLICY_BACKGROUND_FRAMES:
				message = "{background: false}";
				break;
			case Controller.PolicyFlag.POLICY_OPTIMIZE_HMD:
				message = "{optimizeHMD: false}";
				break;
			default:
				Debug.LogError("Unsupported policy flag " + policy + " unsset. Ignoring.");
				return;
			}

			// Send data to server.
			Byte[] sendBytes = Encoding.UTF8.GetBytes(message);
			udpClient.Send (sendBytes, sendBytes.Length);

			// Set internal state
			if (!setPolicies.ContainsKey (policy)) {
				setPolicies.Add (policy, false);
			} else {
				setPolicies[policy] = false;
			}
		}

		// Returns whether a the specified PolicyFlag has been set.
		public bool IsPolicySet(Controller.PolicyFlag policy) {
			bool set;
			
			if (setPolicies.TryGetValue (policy, out set)) {
				return set;
			}
			
			return false;
		}

		// Normally this sends a message to the server enabling/disabling specific gestures, but the JSON API enables all or disables all, so we internally ignore unsupported gestures.
		public void EnableGesture(Gesture.GestureType type, bool enable) {
			// Send message to controller indicating that we want to enable gestures in general.
			String message = "{enableGestures: true}";
			Byte[] sendBytes = Encoding.UTF8.GetBytes(message);
			udpClient.Send (sendBytes, sendBytes.Length);

			// Set internal state
			if (!enabledGestures.ContainsKey (type)) {
				enabledGestures.Add (type, enable);
			} else {
				enabledGestures[type] = enable;
			}
		}

		// Basically an aliased method that enables as a gesture as opposed to enabling or disabling it.
		public void EnableGesture(Gesture.GestureType type) {
			EnableGesture (type, true);
		}

		// Determines whether the specified GestureType is currently enabled for this session.
		public bool IsGestureEnabled(Gesture.GestureType type) {
			bool enabled;
			
			if (enabledGestures.TryGetValue (type, out enabled)) {
				return enabled;
			}
			
			return false;
		}

		// Gets the most current frame of data from the controller and generates all of the other objects from it.
		public Frame Frame() {
			if(udpServer.Available == 0) {
				return new Frame();
			}

			// Use an asynchronous receive to get frame data from the server.
			Profiler.BeginSample("GetUDPDatagram");

			//UDPReceiveState receiveState = new UDPReceiveState(udpServer, remoteEP);
			//udpServer.BeginReceive(new AsyncCallback(ReceiveCallback), receiveState);

			// Time-out the frame if a receive takes too long.
			//float startTime = Time.realtimeSinceStartup;

			//while(!dataReady) {
			//	if(Time.realtimeSinceStartup - startTime > 0.1) {
			//		//Debug.Log("Frame timed out. Dropping.");
			//		return new Frame();
			//	}
			//}

			// Reset this flag for the next frame.
			dataReady = false;

			//byte[] data = receiveState.receivedBytes;

			byte[] data = udpServer.Receive(ref remoteEP);

			//byte[] data = udpServer.Receive(ref remoteEP);

			Profiler.EndSample();
			
			// Extract all fields from byte data and create all classes.
			Profiler.BeginSample("ProcessUDPDatagram");

			MemoryStream memory = new MemoryStream(data);
			BinaryReader reader = new BinaryReader(memory);
			
			// Deserialize frame data
			Frame frame = new Frame();
			frame.CurrentFramesPerSecond = reader.ReadSingle();
			frame.Id = reader.ReadInt64();
			frame.IsValid = true;
			frame.Timestamp = reader.ReadInt64();

			// Detect a junk data frame
			if(frame.CurrentFramesPerSecond < 0 || frame.CurrentFramesPerSecond > 200) {
				Debug.LogWarning ("Junk frame detected. Returning invalid frame.");
				return new Frame();
			}
			
			// Determine how many hands and pointables this frame contains.
			int numHands = reader.ReadInt32();
			int numPointables = reader.ReadInt32();

			string framedbgmsg = "Got frame with ID " + frame.Id + ", hand IDs: ";

			// Deserialize this frame's hands.
			for(int i = 0; i < numHands; i++) {
				Hand hand = new Hand();

				// Deserialize this hand's arm.
				Arm arm = new Arm();
				arm.Basis = getMatrix(reader);
				arm.Center = getVector(reader);
				arm.Direction = getVector(reader);
				arm.ElbowPosition = getVector(reader);
				arm.IsValid = true;
				arm.Width = reader.ReadSingle();
				arm.WristPosition = getVector(reader);

				// Set remainder of hand's properties.
				hand.Arm = arm;
				hand.Confidence = reader.ReadSingle();
				hand.Direction = getVector(reader);
				hand.Frame = frame;
				hand.GrabStrength = reader.ReadSingle();
				hand.Id = reader.ReadInt32();
				hand.IsLeft = getBool(reader);
				hand.IsRight = !hand.IsLeft;
				hand.IsValid = true;
				hand.PalmNormal = getVector(reader);
				hand.PalmPosition = getVector(reader);
				hand.PalmVelocity = getVector(reader);
				hand.PalmWidth = reader.ReadSingle();
				hand.PinchStrength = reader.ReadSingle();
				hand.SphereCenter = getVector(reader);
				hand.SphereRadius = reader.ReadSingle();
				hand.StabilizedPalmPosition = getVector(reader);
				hand.TimeVisible = reader.ReadSingle();
				hand.WristPosition = getVector(reader);

				frame.Hands.Append(hand);

				framedbgmsg += " " + hand.Id;
			}

			// Deserialize this frame's pointables.
			for(int i = 0; i < numPointables; i++) {
				Pointable pointable = new Pointable();
				
				pointable.Direction = getVector(reader);
				pointable.Frame = frame;

				pointable.Hand = frame.Hand(reader.ReadInt32());

				pointable.Id = reader.ReadInt32();
				pointable.IsExtended = getBool(reader);
				pointable.IsFinger = getBool(reader);
				pointable.IsTool = !pointable.IsFinger;
				pointable.IsValid = true;
				pointable.Length = reader.ReadSingle();
				pointable.StabilizedTipPosition = getVector(reader);
				pointable.TimeVisible = reader.ReadSingle();
				pointable.TipPosition = getVector(reader);
				pointable.TipVelocity = getVector(reader);
				pointable.TouchDistance = reader.ReadSingle();
				
				int touchZone = reader.ReadInt32();
				switch(touchZone) {
				case 0:
					pointable.TouchZone = Pointable.Zone.ZONE_NONE;
					break;
				case 1:
					pointable.TouchZone = Pointable.Zone.ZONE_HOVERING;
					break;
				case 2:
					pointable.TouchZone = Pointable.Zone.ZONE_TOUCHING;
					break;
				default:
					Debug.LogError("Invalid Zone ordinal: " + touchZone);
					break;
				}
				
				pointable.Width = reader.ReadSingle();
				
				// Apply specific properties to tools or fingers.
				if(pointable.IsTool) {
					Tool tool = new Tool(pointable);
					frame.Tools.Append(tool);
					pointable.Hand.Tools.Append(tool);
				} else {
					Finger finger = new Finger(pointable);

					int fingerType = reader.ReadInt32();
					switch(fingerType) {
					case 0:
						finger.Type = Finger.FingerType.TYPE_THUMB;
						break;
					case 1:
						finger.Type = Finger.FingerType.TYPE_INDEX;
						break;
					case 2:
						finger.Type = Finger.FingerType.TYPE_MIDDLE;
						break;
					case 3:
						finger.Type = Finger.FingerType.TYPE_RING;
						break;
					case 4:
						finger.Type = Finger.FingerType.TYPE_PINKY;
						break;
					default:
						Debug.LogError("Invalid FingerType ordinal: " + fingerType);
						break;
					}
					
					// Get bones for this finger.
					for(int j = 0; j < 4; j++) {
						Bone bone = new Bone();
						bone.Basis = getMatrix(reader);
						bone.Center = getVector(reader);
						bone.Direction = getVector(reader);
						bone.IsValid = true;
						bone.Length = reader.ReadSingle();
						bone.NextJoint = getVector(reader);
						bone.PrevJoint = getVector(reader);
						
						int boneType = reader.ReadInt32();
						switch(boneType) {
						case 0:
							bone.Type = Bone.BoneType.TYPE_METACARPAL;
							break;
						case 1:
							bone.Type = Bone.BoneType.TYPE_PROXIMAL;
							break;
						case 2:
							bone.Type = Bone.BoneType.TYPE_INTERMEDIATE;
							break;
						case 3:
							bone.Type = Bone.BoneType.TYPE_DISTAL;
							break;
						default:
							Debug.LogError("Invalid BoneType ordinal: " + boneType);
							break;
						}
						//Debug.Log(boneType);
						
						bone.Width = reader.ReadSingle();
						
						finger.bones.Insert(boneType, bone);
					}

					frame.Fingers.Append(finger);
					pointable.Hand.Fingers.Append(finger);
				}
				
				frame.Pointables.Append(pointable);
				pointable.Hand.Pointables.Append(pointable);
			}
			Profiler.EndSample();


			//Debug.Log(framedbgmsg);

			// Add the frame to the history list.
			//frameHistory [currentFront] = frame;
			//currentFront = ++currentFront % MAX_HISTORY_SIZE;

			// Close opened streams
			reader.Close();
			memory.Close();

			return frame;
		}

		// Returns the frame at the specific point in time. An argument of N will return the Nth frame before the current frame.
		public Frame Frame(int history) {
			if (history < 0 || history >= MAX_HISTORY_SIZE) {
				// Return an invalid frame.
				return new Frame();
			}

			Frame frame = frameHistory [history];

			if (frame == null) {
				// This usually happens when we've just started storing frames in history and the user goes back too far in history.
				return new Frame();
			}

			return frame;
		}

		// Returns true when connected to the leap motion service. We'll always say this is true.
		public bool IsServiceConnected() {
			return true;
		}

		// Returns the current system time in microseconds.
		public long Now() {
			return (long)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds * 1000000;
		}

		/* PRIVATE HELPER METHODS */

		// Asynchornous callback to receive a UDP datagram.
		private static void ReceiveCallback(IAsyncResult asyncResult) {
			UDPReceiveState receiveState = (UDPReceiveState) asyncResult.AsyncState;

			try {
				UdpClient client = receiveState.client;
				IPEndPoint remoteHost = receiveState.remoteHost;

				byte[] result = client.EndReceive(asyncResult, ref remoteHost);

				receiveState.receivedBytes = result;

				dataReady = true;
			} catch (Exception e) {
				Debug.LogError("Receive was not successful: " + e.ToString());
				//receiveState.receivedBytes = null;
			}
		}

		// Reads a sequence of three floats and returns a Leap Vector.
		private Vector getVector(BinaryReader reader) {
			return new Vector(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
		}

		// Reads a sequence of nine floats and returns a Leap Matrix.
		private Matrix getMatrix(BinaryReader reader) {
			Vector xBasis = getVector(reader);
			Vector yBasis = getVector(reader);
			Vector zBasis = getVector(reader);
			
			return new Matrix(xBasis, yBasis, zBasis);
		}
		
		public bool getBool(BinaryReader reader) {
			byte boolean = reader.ReadByte();
			
			return (boolean == 1) ? true : false;
		}
	}

	public class UDPReceiveState {
		public UdpClient client;
		public IPEndPoint remoteHost;
		public byte[] receivedBytes;

		public UDPReceiveState(UdpClient _client, IPEndPoint _remoteHost) {
			client = _client;
			remoteHost = _remoteHost;
		}
	}
}
