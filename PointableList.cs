using UnityEngine;
using System.Collections;
using System;

namespace UDPLeap {
	public class PointableList {
		/* PUBLIC PROPERTIES */
		
		public int Count
		{
			get
			{
				return pointableList.Count;
			}
		}
		
		public bool IsEmpty
		{
			get
			{
				return (pointableList.Count == 0);
			}
		}

		// Pointable with smallest z co-ordinate
		public Pointable Frontmost
		{
			get
			{
				Pointable frontmost = new Pointable();
				float minZ = float.MaxValue;
				float currentZ;
				
				foreach (Pointable pointable in pointableList) {
					currentZ = pointable.TipPosition.z;
					if (currentZ < minZ) {
						minZ = currentZ;
						frontmost = pointable;
					}
				}
				
				return frontmost;
			}
		}
		
		// Pointable with smallest x co-ordinate
		public Pointable Leftmost
		{
			get
			{
				Pointable leftmost = new Pointable();
				float minX = float.MaxValue;
				float currentX;
				
				foreach (Pointable pointable in pointableList) {
					currentX = pointable.TipPosition.x;
					if (currentX < minX) {
						minX = currentX;
						leftmost = pointable;
					}
				}
				
				return leftmost;
			}
		}
		
		// Pointable with largest x co-ordinate
		public Pointable Rightmost
		{
			get
			{
				Pointable rightmost = new Pointable();
				float maxX = float.MinValue;
				float currentX;
				
				foreach(Pointable pointable in pointableList) {
					currentX = pointable.TipPosition.x;
					if (currentX > maxX) {
						maxX = currentX;
						rightmost = pointable;
					}
				}
				
				return rightmost;
			}
		}
		
		/* IMPLEMENTATION-SPECIFIC PROPERTIES */
		
		private ArrayList pointableList;

		/* PUBLIC METHODS */
		
		// Constructor creates a new empty list of Pointables.
		public PointableList() {
			pointableList = new ArrayList ();
		}


		// Make an array accessor
		public Pointable this[int index] {
			get {
				try {
					return (Pointable) pointableList[index];
				} catch (ArgumentOutOfRangeException) {
					// Return an invalid object if we're out of range.
					return new Pointable();
				}
			}
		}

		// Returns an iterator

		// Add an item to the Pointable list.
		public PointableList Append(Pointable pointable) {
			pointableList.Add (pointable);

			return this;
		}

		// Appends the items of otherList to the end of this list.
		public PointableList Append(PointableList otherList) {
			pointableList.AddRange (otherList.pointableList);
			
			return this;
		}

		// Appends the items of otherList to the end of this list.
		public PointableList Append(FingerList otherList) {
			pointableList.AddRange (otherList.fingerList);
			
			return this;
		}

		// Appends the items of otherList to the end of this list.
		public PointableList Append(ToolList otherList) {
			pointableList.AddRange (otherList.toolList);
			
			return this;
		}

		// Return an enumerator on this list.
		public IEnumerator GetEnumerator()
		{
			return pointableList.GetEnumerator();
		}

		// Returns a list of all extended pointables
		public PointableList Extended() {
			PointableList extendedPointables = new PointableList();

			foreach (Pointable pointable in pointableList) {
				if(pointable.IsExtended) {
					extendedPointables.Append(pointable);
				}
			}

			return extendedPointables;
		}
	}
}
