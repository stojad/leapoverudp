﻿using UnityEngine;
using System.Collections;

namespace UDPLeap {
	public class Pointable {
		/* PUBLIC PROPERTIES */

		public Vector Direction;
		public Frame Frame;
		public Hand Hand;
		public int Id;

		public Pointable Invalid
		{
			get
			{
				return new Pointable();
			}
		}

		public bool IsExtended;
		public bool IsFinger = false;
		public bool IsTool = false;
		public bool IsValid = false;
		public float Length;
		public Vector StabilizedTipPosition;
		public float TimeVisible;
		public Vector TipPosition;
		public Vector TipVelocity;
		public float TouchDistance;
		public Pointable.Zone TouchZone;
		public float Width;
		public enum Zone {ZONE_NONE, ZONE_HOVERING, ZONE_TOUCHING};

		/* PUBLIC METHODS */

		// Creates an invalid Pointable.
		public Pointable() {
		
		}

		// Two Pointable objects are equal if they have the same ID, come from the same frame and are both valid.
		public bool Equals(Pointable arg0) {
			return (IsValid && arg0.IsValid && Id == arg0.Id && Frame.Id == arg0.Frame.Id);
		}
	}
}
