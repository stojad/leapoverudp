﻿using UnityEngine;
using System.Collections;

namespace UDPLeap {
	public class Gesture {
		/* PUBLIC PROPERTIES */

		public enum GestureType
		{
			TYPE_INVALID = -1,
			TYPE_SWIPE = 1,
			TYPE_CIRCLE = 4,
			TYPE_SCREEN_TAP = 5,
			TYPE_KEY_TAP = 6,
		};
	}
}
