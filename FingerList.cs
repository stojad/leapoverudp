﻿using UnityEngine;
using System.Collections;
using System;

namespace UDPLeap {
	public class FingerList {
		/* PUBLIC PROPERTIES */
		
		public int Count
		{
			get
			{
				return fingerList.Count;
			}
		}
		
		public bool IsEmpty
		{
			get
			{
				return (fingerList.Count == 0);
			}
		}
		
		// Finger with smallest z co-ordinate
		public Finger Frontmost
		{
			get
			{
				Finger frontmost = new Finger();
				float minZ = float.MaxValue;
				float currentZ;
				
				foreach (Finger finger in fingerList) {
					currentZ = finger.TipPosition.z;
					if (currentZ < minZ) {
						minZ = currentZ;
						frontmost = finger;
					}
				}
				
				return frontmost;
			}
		}
		
		// Finger with smallest x co-ordinate
		public Finger Leftmost
		{
			get
			{
				Finger leftmost = new Finger();
				float minX = float.MaxValue;
				float currentX;
				
				foreach (Finger finger in fingerList) {
					currentX = finger.TipPosition.x;
					if (currentX < minX) {
						minX = currentX;
						leftmost = finger;
					}
				}
				
				return leftmost;
			}
		}
		
		// Hand with largest x co-ordinate
		public Finger Rightmost
		{
			get
			{
				Finger rightmost = new Finger();
				float maxX = float.MinValue;
				float currentX;
				
				foreach(Finger finger in fingerList) {
					currentX = finger.TipPosition.x;
					if (currentX > maxX) {
						maxX = currentX;
						rightmost = finger;
					}
				}
				
				return rightmost;
			}
		}
		
		/* IMPLEMENTATION-SPECIFIC PROPERTIES */
		
		public ArrayList fingerList;
		
		/* PUBLIC METHODS */
		
		// Constructor creates a new empty list of Fingers.
		public FingerList() {
			fingerList = new ArrayList ();
		}

		// Makes an array accessor.
		public Finger this[int index] {
			get {
				try {
					return (Finger) fingerList[index];
				} catch (ArgumentOutOfRangeException) {
					// Return an invalid object if we're out of range.
					return new Finger();
				}
			}
		}

		// Return an enumerator on this list.
		public IEnumerator GetEnumerator()
		{
			return fingerList.GetEnumerator();
		}
		
		// Add an item to the Finger list.
		public FingerList Append(Finger finger) {
			fingerList.Add (finger);
			
			return this;
		}
		
		// Appends the items of otherList to the end of this list.
		public FingerList Append(FingerList otherList) {
			fingerList.AddRange (otherList.fingerList);
			
			return this;
		}

		// Returns a list of all extended pointables
		public FingerList Extended() {
			FingerList extendedFingers = new FingerList();
			
			foreach (Finger finger in fingerList) {
				if(finger.IsExtended) {
					extendedFingers.Append(finger);
				}
			}
			
			return extendedFingers;
		}
	}
}
