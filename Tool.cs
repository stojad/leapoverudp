﻿using UnityEngine;
using System.Collections;

namespace UDPLeap {
	public class Tool : Pointable {
		/* PUBLIC PROPERTIES */
		
		new Tool Invalid 
		{
			get
			{
				return new Tool();
			}
		}
	
		
		/* PUBLIC METHODS */
		
		// Construct an invalid Tool.
		public Tool() {
			
			
		}
		
		// Construct a Tool from a Pointable object.
		public Tool(Pointable arg0) {
			// Create an invalid Tool object if the Pointable arg0 is not a Finger.
			if (!arg0.IsValid || !arg0.IsTool) {
				IsValid = false;
				return;
			}
			
			// Copy all fields.
			Direction = arg0.Direction;
			Frame = arg0.Frame;
			Hand = arg0.Hand;
			Id = arg0.Id;
			IsExtended = arg0.IsExtended;
			IsFinger = arg0.IsFinger;
			IsTool = arg0.IsTool;
			IsValid = arg0.IsValid;
			Length = arg0.Length;
			StabilizedTipPosition = arg0.StabilizedTipPosition;
			TimeVisible = arg0.TimeVisible;
			TipPosition = arg0.TipPosition;
			TipVelocity = arg0.TipVelocity;
			TouchDistance = arg0.TouchDistance;
			TouchZone = arg0.TouchZone;
			Width = arg0.Width;
		}
	}
}
