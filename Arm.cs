﻿using UnityEngine;
using System.Collections;

namespace UDPLeap {
	public class Arm {
		/* PUBLIC PROPERTIES */
	
		public Matrix Basis;
		public Vector Center;
		public Vector Direction;
		public Vector ElbowPosition;

		public Arm Invalid
		{
			get
			{
				return new Arm();
			}

		}
		public bool IsValid = false;
		public float Width;
		public Vector WristPosition;

		/* PUBLIC METHODS */

		// Creates an invalid arm.
		public Arm() {

		}

		// public bool Equals(object arg0);

		// Provide a string representation of the arm.
		public override string ToString () {
			return "[Arm] Elbow Position: " + ElbowPosition.ToString() + ", wrist position: " + WristPosition.ToString();
		}
	}
}
