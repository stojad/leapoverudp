using UnityEngine;
using System.Collections;
using System;

namespace UDPLeap {
    // Warning: this class contains advanced black magic and should not be viewed by the uninitiated.
	public class Matrix {
	   /* PUBLIC PROPERTIES */
       
       public Matrix Identity
       {
           get
           {
                return new Matrix();    
           }
       }
       
       public Vector xBasis
       {
            get
            {
                return new Vector(matrix.GetColumn(0));
            }
           
            set
            {
                matrix.SetColumn(0, value.ToUnityVector4());
            }   
       }
       
        public Vector yBasis
        {
            get
            {
                return new Vector(matrix.GetColumn(1));
            }
           
            set
            {
                matrix.SetColumn(1, value.ToUnityVector4());
            }   
        }
       
        public Vector zBasis
        {
            get
            {
                return new Vector(matrix.GetColumn(2));
            }
           
            set
            {
                matrix.SetColumn(2, value.ToUnityVector4());
            }   
        }
       
        public Vector origin
        {
            get
            {
                return new Vector(matrix.GetColumn(3));
            } 
           
            set
            {
                matrix.SetColumn(3, value.ToUnityPositionVector4());
            }   
        }
       
       /* IMPLEMENTATION-SPECIFIC PROPERTIES */
       
        private Matrix4x4 matrix;
        
        /* PUBLIC METHODS */
        
        // Construct an identity matrix
        public Matrix() {
            matrix = Matrix4x4.identity;
        }
        
        // Copy constructor. The matrix itself doesn't have a copy constructor, so we implm
        public Matrix(Matrix other) {
			matrix = Matrix4x4.identity;

			xBasis = new Vector (other.xBasis);
			yBasis = new Vector (other.yBasis);
			zBasis = new Vector (other.zBasis);
			origin = new Vector (other.origin);
        }
        
        // Construct a transformation matrix using three basis columns (i.e. no translation)
        public Matrix(Vector _xBasis, Vector _yBasis, Vector _zBasis) {
            matrix = Matrix4x4.identity;
            
            xBasis = _xBasis;
            yBasis = _yBasis;
            zBasis = _zBasis;
        }
        
         // Construct a transformation matrix using three basis columns and a translation
        public Matrix(Vector _xBasis, Vector _yBasis, Vector _zBasis, Vector _origin) {
            matrix = Matrix4x4.identity;
            
            xBasis = _xBasis;
            yBasis = _yBasis;
            zBasis = _zBasis;
            origin = _origin;
        }
        
        // Construct a transformation matrix using angle-axis rotation (i.e. no translation)
        public Matrix(Vector axis, float angleRadians) {
			Quaternion rotation = Quaternion.AngleAxis((float) (180/Math.PI) * angleRadians, axis.ToUnityVector3());
            Vector3 scale = new Vector3(1, 1, 1);
            Vector3 translation = new Vector3(0, 0, 0);
            
            matrix = Matrix4x4.TRS(translation, rotation, scale).transpose;
        }
        
         // Construct a transformation matrix using angle-axis rotation and a translation
        public Matrix(Vector axis, float angleRadians, Vector _translation) {
			Quaternion rotation = Quaternion.AngleAxis((float) (180/Math.PI) * angleRadians, axis.ToUnityVector3());
            Vector3 scale = new Vector3(1, 1, 1);
			Vector3 translation = new Vector3(0, 0, 0);
            
            matrix = Matrix4x4.TRS(translation, rotation, scale).transpose;

			origin = _translation;
        }
        
        // Return the inverse of this matrix.
        public Matrix RigidInverse() {
			Matrix inverse = new Matrix ();
			inverse.matrix = matrix.inverse;

			return inverse;
        }
        
        // Set the rotation of a matrix but don't change its translation.
        public void SetRotation(Vector axis, float angleRadians) {
            Quaternion rotation = Quaternion.AngleAxis((float) (180 / Math.PI ) * angleRadians, axis.ToUnityVector3());
            Vector3 scale = Vector3.one;
			Vector3 translation = Vector3.zero;

			// Preserve the original translation
			Vector oldTranslation = new Vector(origin);
            
			matrix = Matrix4x4.TRS(translation, rotation, scale).transpose;

			origin = oldTranslation;
            
        }
        
        // Transform a vector using this matrix (scale and direction ONLY). 
		public Vector TransformDirection(Vector arg0) {
            // Perform the full transform and subtract the translation
            return new Vector(matrix.MultiplyPoint3x4(arg0.ToUnityVector3())) - origin;
		}
        
        // Perform a general projective transformation on a point
		public Vector TransformPoint(Vector point) {
	        return new Vector(matrix.MultiplyPoint3x4(point.ToUnityVector3()));
	   }
       
       // Define matrix multiplication. Just uses the Unity Matrix4x4 multiplication operator overload.
       public static Matrix operator *(Matrix A, Matrix B) {
			Matrix C = new Matrix();
			C.matrix = A.matrix * B.matrix;
			return C;
       }
       
       // Make a nice string representation of the matrix
       public override string ToString() {
			return "xBasis: " + xBasis.ToString() + " yBasis: " + yBasis.ToString() + " zBasis: " + zBasis.ToString() + " origin: " + origin.ToString() + " matrix:\n" + unityMatrixString(matrix);
       }
       
        /* IMPLEMENTATION-SPECIFIC METHODS */
        
        // Converts the transformation matrix to a quaternion, useful for determing rotation factors elsewhere in the API.
        // This isn't explicitly defined by the API.
        // Adapted from: http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/index.htm
		public Quaternion QuaternionFromMatrix() {
		    Quaternion q = new Quaternion();
		    q.w = Mathf.Sqrt( Mathf.Max( 0, 1 + matrix[0,0] + matrix[1,1] + matrix[2,2] ) ) / 2; 
		    q.x = Mathf.Sqrt( Mathf.Max( 0, 1 + matrix[0,0] - matrix[1,1] - matrix[2,2] ) ) / 2; 
		    q.y = Mathf.Sqrt( Mathf.Max( 0, 1 - matrix[0,0] + matrix[1,1] - matrix[2,2] ) ) / 2; 
		    q.z = Mathf.Sqrt( Mathf.Max( 0, 1 - matrix[0,0] - matrix[1,1] + matrix[2,2] ) ) / 2; 
		    q.x *= Mathf.Sign( q.x * ( matrix[2,1] - matrix[1,2] ) );
		    q.y *= Mathf.Sign( q.y * ( matrix[0,2] - matrix[2,0] ) );
		    q.z *= Mathf.Sign( q.z * ( matrix[1,0] - matrix[0,1] ) );
		    return q;
		}

		// Nicer pretty printer for the Unity matrix class
		public string unityMatrixString(Matrix4x4 matrix) {
			String outputString = "";

			for(int i = 0; i < 4; i++) {
				for(int j = 0; j < 4; j++) {
					outputString += matrix[i, j].ToString("F4") + " ";
				}

				outputString += "\n";
			}

			return outputString;
		}
   }
}
