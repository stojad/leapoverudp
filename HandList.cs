using UnityEngine;
using System.Collections;
using System;

namespace UDPLeap {
	public class HandList {
		/* PUBLIC PROPERTIES */
		
		public int Count
		{
			get
			{
				return handList.Count;
			}
		}
		
		public bool IsEmpty
		{
			get
			{
				return (handList.Count == 0);
			}
		}
		
		// Hand with smallest z co-ordinate
		public Hand Frontmost
		{
			get
			{
				Hand frontmost = new Hand();
				float minZ = float.MaxValue;
				float currentZ;
				
				foreach (Hand hand in handList) {
					currentZ = hand.PalmPosition.z;
					if (currentZ < minZ) {
						minZ = currentZ;
						frontmost = hand;
					}
				}
				
				return frontmost;
			}
		}
		
		// Hand with smallest x co-ordinate
		public Hand Leftmost
		{
			get
			{
				Hand leftmost = new Hand();
				float minX = float.MaxValue;
				float currentX;
				
				foreach (Hand hand in handList) {
					currentX = hand.PalmPosition.x;
					if (currentX < minX) {
						minX = currentX;
						leftmost = hand;
					}
				}
				
				return leftmost;
			}
		}
		
		// Hand with largest x co-ordinate
		public Hand Rightmost
		{
			get
			{
				Hand rightmost = new Hand();
				float maxX = float.MinValue;
				float currentX;
				
				foreach(Hand hand in handList) {
					currentX = hand.PalmPosition.x;
					if (currentX > maxX) {
						maxX = currentX;
						rightmost = hand;
					}
				}
				
				return rightmost;
			}
		}
		
		/* IMPLEMENTATION-SPECIFIC PROPERTIES */
		
		private ArrayList handList;


		/* PUBLIC METHODS */
		
		// Constructor creates a new empty list of hands.
		public HandList() {
			handList = new ArrayList ();
		}

		// Makes an array accessor.
		public Hand this[int index] {
			get {
				try {
					return (Hand) handList[index];
				} catch (ArgumentOutOfRangeException) {
					// Return an invalid object if we're out of range.
					return new Hand();
				}
			}
		}

		// Return an enumerator on this list.
		public IEnumerator GetEnumerator()
		{
			return handList.GetEnumerator();
		}

		// Add an item to the hand list.
		public HandList Append(Hand hand) {
			handList.Add (hand);

			return this;
		}
	}
}
