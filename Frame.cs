﻿using UnityEngine;
using System.Collections;

namespace UDPLeap {
	public class Frame {
		/* PUBLIC PROPERTIES */

		public double CurrentFramesPerSecond;
		public FingerList Fingers = new FingerList();
		public HandList Hands = new HandList();
		public long Id;
		// public ImageList Images
		// public InteractionBox InteractionBox

		public Frame Invalid
		{
			get
			{
				return new Frame();
			}
		}

		public bool IsValid = false;
		public PointableList Pointables = new PointableList();
		// public byte[] Serialize;
		public long Timestamp;
		public ToolList Tools = new ToolList();
		// public TrackedQuad TrackedQuad;


		/* PUBLIC METHODS */

		// public void Deserialize(byte[] arg);

		// Checks two frames for equality
		public bool Equal(Frame arg0) {
			return (IsValid && arg0.IsValid && Id == arg0.Id);
		}

		// Returns a finger from the frame
		public Finger Finger(int id) {
			foreach (Finger finger in Fingers) {
				if(finger.Id == id) {
					return finger;
				}
			}

			// Return an invalid object if no object of that ID was found.
			return new Finger();
		}

		// Construct an invalid Frame object
		public Frame() {

		}

		// public Gesture(int id);

		// public GestureList Gestures();

		// public GestureList Gestures(Frame sinceFrame);

		// Return a hand with the specified ID.
		public Hand Hand(int id) {
			foreach (Hand hand in Hands) {
				if(hand.Id == id) {
					return hand;
				}
			}

			// Return an invalid object if no object of that ID was found.
			return new Hand ();
		}

		// Return a Pointable with the speciifed ID
		public Pointable Pointable(int id) {
			foreach (Pointable pointable in Pointables) {
				if(pointable.Id == id) {
					return pointable;
				}
			}

			// Return an invalid object if no object of that ID was found.
			return new Pointable();
		}

		// float RotationAngle (Frame sinceFrame);

		// float RotationAngle(Frame sinceFrame, Vector axis);

		// Vector RotationAxis(Frame sinceFrame);

		// Matrix RotationMatrix(Frame sinceFrame);

		// float RotationProbability(Frame sinceFrame);

		// float ScaleFactor(Frame sinceFrame);

		// float ScaleProbability(Frame sinceFrame);

		public Tool Tool(int id) {
			foreach (Tool tool in Tools) {
				if(tool.Id == id) {
					return tool;
				}
			}

			// Return in invalid object if no object of that ID was found.
			return new Tool ();
		}
	
		// Return a string representation of this Frame.
		public override string ToString ()
		{
			return string.Format ("[Frame: IsValid={0}, Hands.Count={1}, Pointables.Count={2}, Fingers.Count={3}, Tools.Count={4}]", IsValid, Hands.Count, Pointables.Count, Fingers.Count, Tools.Count);
		}

		// public Vector Translation(Frame sinceFrame);

		// public float TranslationProbability(Frame sinceFrame);
	}
}
