using UnityEngine;
using System.Collections;
using System;

namespace UDPLeap {
	public class Vector {
		/* PUBLIC PROPERTIES */
		
		public float x, y, z;
		
		public Vector Up
		{
			get 
			{
				return new Vector(0, 1, 0);
			}
		}
		
		public Vector Down
		{
			get 
			{
				return new Vector(0, -1, 0);
			}
		}
		
		public Vector Right
		{
			get 
			{
				return new Vector(1, 0, 0);
			}
		}
		
		public Vector Left
		{
			get 
			{
				return new Vector(-1, 0, 0);
			}
		}
		
		public Vector Backward
		{
			get 
			{
				return new Vector(0, 0, 1);
			}
		}
		
		public Vector Forward
		{
			get 
			{
				return new Vector(0, 0, -1);
			}	
		}
		
		public float Pitch {
			get 
			{
				// Project this vector onto yz plane, which has a normal in the positive x direction.
				Vector yzproj = Proj(XAxis);
				
				// Magnitude of the pitch is the angle between the negative z axis and the projection.
				float angle = Forward.AngleTo(yzproj);
				
				// Vector is pointing 'up' if dot product with positive y axis is positive.
				// The pitch angle is positive if vector is pointing up.
				return (yzproj.Dot(Up) > 0) ? angle : -angle;
			}
		}
		
		public float Roll {
			get 
			{
				// Project this vector onto xy plane, which has a normal in the positive z direction.
				Vector xyproj = Proj(ZAxis);
				
				// Magnitude of the roll angle is the angle between the positive y axis and the projection.
				float angle = Down.AngleTo(xyproj);
				
				// Vector is pointing 'to the left' if dot product with negative x axis is positive.
				// The pitch angle is positive if vector is pointing to the left.
				return (xyproj.Dot(Right) > 0) ? angle : -angle;
			}
		}
		
		public float Yaw {
			get 
			{
				// Project this vector onto xz plane, which has a normal in the positive y direction.
				Vector xzproj = Proj(YAxis);
				
				// Magnitude of the roll angle is the angle between the negative z axis and the projection.
				float angle = Forward.AngleTo(xzproj);
				
				// Vector is pointing to the right of the z axis if dot product with positive x axis is positive.
				// The pitch angle is positive if vector is pointing to the right of the z axis.
				return (xzproj.Dot(Right) > 0) ? angle : -angle;		
			}
		}
		
		public Vector XAxis
		{
			get 
			{
				return new Vector(1, 0, 0);
			}
		}
		
		public Vector YAxis
		{
			get 
			{
				return new Vector(0, 1, 0);
			}
		}
		
		public Vector ZAxis
		{
			get 
			{
				return new Vector(0, 0, 1);
			}
		}
		
		public float Magnitude {
			get
			{
				return Mathf.Sqrt(x*x + y*y + z*z);	
			}
		}
		
		public float MagnitudeSquared {
			get
			{
				return x*x + y*y + z*z;	
			}
		}
		
		public Vector Normalized {
			get
			{
				float magnitude = this.Magnitude;
				
				return new Vector(x/magnitude, y/magnitude, z/magnitude);
			}
		}
		
		public Vector Zero {
			get
			{
				return new Vector(0, 0, 0);
			}
		}
		
		
		/* PUBLIC METHODS */
		
		// Creates a zero vector
		public Vector() {
			x = 0;
			y = 0;
			z = 0;
		}
		
		// Creates a new vector (_x, _y, _z)
		public Vector(float _x, float _y, float _z) {
			x = _x;
			y = _y;
			z = _z;
		}
		
		// Copy constructor
		public Vector(Vector vector) {
			x = vector.x;
			y = vector.y;
			z = vector.z;
		}
		
		// Creates a vector from a Unity Vector3.
		public Vector(Vector3 vector) {
			x = vector[0];
			y = vector[1];
			z = vector[2];
		}
		
		// Creates a vector from a Unity Vector4 (discards the last element).
		public Vector(Vector4 vector) {
			x = vector[0];
			y = vector[1];
			z = vector[2];
		}

		// Define the addition operation for two vectors
		public static Vector operator +(Vector vec1, Vector vec2) {
			return new Vector(vec1.x + vec2.x, vec1.y + vec2.y, vec1.z + vec2.z);
		}
		
		// Define the subtraction operation for two vectors
		public static Vector operator -(Vector vec1, Vector vec2) {
			return new Vector(vec1.x - vec2.x, vec1.y - vec2.y, vec1.z - vec2.z);
		}
		
		// Return the negative of a vector
		public static Vector operator -(Vector vec) {
			return new Vector(-vec.x, -vec.y, -vec.z);
		}

		// Define the multiplication of a vector by a scalar
		public static Vector operator *(float scale, Vector vec) {
			return new Vector(scale*vec.x, scale*vec.y, scale*vec.z);
		}
		
		// Same multiplication method but with the vector and scalar reversed in order
		public static Vector operator *(Vector vec, float scale) {
			return new Vector(scale*vec.x, scale*vec.y, scale*vec.z);
		}
		
		// Define division of a vector by a scalar
		public static Vector operator /(Vector vec, float scale) {
			return new Vector(vec.x/scale, vec.y/scale, vec.z/scale);
		}
		
		// Get a human-readable string representation of the vector
		public override string ToString() {
			return "(" + x + ", " + y + ", " + z + ")";
		}
		
		// Return a float array of the vector values, [x, y, z]
		public float[] toFloatArray() {
			return new float[]{x, y, z};
		}
		
		// Returns true if all of the components are finite and not NaN
		public bool IsValid() {
			 return !Single.IsNaN(x) && !Single.IsInfinity(x) && !Single.IsNaN(y) && !Single.IsInfinity(y) && !Single.IsNaN(z) && !Single.IsInfinity(z);
		}
		
		// Test two vectors for equality
		public bool Equals(Vector other) {
			float xdiff = Math.Abs(x - other.x);
			float ydiff = Math.Abs(y - other.y);
			float zdiff = Math.Abs(z - other.z);
			
			float maxdiff = 10e-6f;
			
			return (xdiff < maxdiff) && (ydiff < maxdiff) && (zdiff < maxdiff);
		}
		
		// Dot poduct of two vectors
		public float Dot(Vector other) {
			return x*other.x + y*other.y + z*other.z;
		}
		
		// Angle between two vectors. This is undefined if one of the vectors is a zero vector
		public float AngleTo(Vector other) {
			return Mathf.Acos(Dot(other)/(Magnitude*other.Magnitude));
		}
		
		// Distance between two vectors. I'm assuming they mean Euclidean distance
		public float DistanceTo(Vector other) {
			float xdiff = x - other.x;
			float ydiff = y - other.y;
			float zdiff = z - other.z;
			
			return Mathf.Sqrt(xdiff*xdiff + ydiff*ydiff + zdiff*zdiff);
		}
		
		// Compute the cross product between two vectors
		public Vector Cross(Vector other) {
			return new Vector(y*other.z - z*other.y, z*other.x - x*other.z, x*other.y - y*other.x);
		}
		
		// Converts this vector into a Unity Vector3
		public Vector3 ToUnityVector3() {
			return new Vector3(x, y, z);
		}

		// Converts this vector into a Unity Vector4, making the last element 0 for the rotation bases.
		public Vector4 ToUnityVector4() {
			return new Vector4(x, y, z, 0);
		}

		// Converts this vector into a Unity Vector4, making the last element 1 for the translation vector.
		public Vector4 ToUnityPositionVector4() {
			return new Vector4(x, y, z, 1);
		}
		
		/* PRIVATE METHODS */
		
		// Computes the vector projection of this vector onto a vector n. Not explicitly supported by the API
		private Vector Proj(Vector n) {
			return this - Dot(n)/n.Dot(n) * n;
		}
	}
}
